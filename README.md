# This is an example project that show how to use Review Apps

This is a very basic project that has one static page, but it shows how to use
the environments feature of GitLab and the recently introduced dynamic
environments which can be used for Review Apps.

Review Apps allow you to create a new environment for each one of your branches.
This review app can then be visible as a link when you visit the merge request
relevant to the branch. That way you are able to see live all changes
introduced by the merge request changes.

The example here uses a set of 3 environments:

1. `production`: you trigger to production manually, by clicking the **Production**
   action under the **Pipelines** tab of your project.
1. `staging`: the staging is deployed automatically when changes to master get
   merged.
1. `review/*`: the review is created on any other branch that is pushed to GitLab.

## Access the example

This project can be accessed under this addresses:

1. `production`: http://production.138.68.69.232.nip.io/
1. `staging`: http://staging.138.68.69.232.nip.io/
1. `review` for merge request [!1](https://gitlab.com/gitlab-examples/review-apps-nginx/merge_requests/1): http://update-hello-world.138.68.69.232.nip.io/

The review app can be seen in [Merge Request !1](https://gitlab.com/gitlab-examples/review-apps-nginx/merge_requests/1)
which is open:

<img src="images/merge_request.png" width="600">

---

Or in [Merge Request !2](https://gitlab.com/gitlab-examples/review-apps-nginx/merge_requests/1)
which is merged:

<img src="images/merge_merged.png" width="600">

## Use it for your projects

This is a very simple example, but you can adapt it for your needs and have a
page that is deployed dynamically.

To do that you have to follow these few simple steps.

1. [Install GitLab Runner](https://docs.gitlab.com/runner/install/linux-repository.html)
1. [Obtain a Runner token](https://docs.gitlab.com/ee/ci/runners/#registering-a-specific-runner-with-a-project-registration-token)
1. [Register the Runner](https://docs.gitlab.com/runner/register/#gnu-linux). Use the `shell` executor and assign a few tags: `nginx`, `review-apps`, `deploy`.
1. Configure your server:

    The script was tested on Ubuntu 18.04:

    ```sh
    IP=$(dig +short myip.opendns.com @resolver1.opendns.com)
    sudo apt-get update
    sudo apt-get install -y nginx
    sudo mkdir -p /srv/nginx/pages
    sudo chown -R gitlab-runner /srv/nginx/pages

    cat << EOF | sudo tee /etc/nginx/sites-available/dynamic-pages
    server {
        listen 80;
        server_name ~^(www\.)?(?<sname>.+?).$IP.nip.io$;
        root /srv/nginx/pages/$sname/public;

        index index.html index.htm index.php;

        charset utf-8;

        location / {
            try_files $uri $uri/ /index.html;
        }

        access_log /var/log/nginx/$sname-access.log;
        error_log  /var/log/nginx/pages-error.log debug;
    }
    EOF
    sudo ln -s /etc/nginx/sites-{available,enabled}/dynamic-pages
    sudo rm -f /etc/nginx/sites-enabled/default
    sudo systemctl restart nginx
    ```

    **You can optionally replace `$IP.nip.io` with your wildcard DNS record**

1. Set up a variable `APPS_DOMAIN` in the CI/CD settings pointing to the domain (`1.2.3.4.nip.io`)
1. Modify `.gitlab-ci.yml`

    Check out other examples of static page generators: https://gitlab.com/groups/pages
    and incorporate them into `.gitlab-ci.yml`

You can now start pushing your changes and see them live!
